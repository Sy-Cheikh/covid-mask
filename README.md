# covid mask
## What do you look like without a covid mask?





## Name
covid-mask

## Description
With the Covid-19 pandemic, we have learned to wear masks on a daily basis. 
Now half of our face is hidden, leaving only the eyes as a visible means of expression. This operation of restoring the face is similar to a field in image and video processing called *inpainting*. Indeed, the pixels corresponding to the mask area (modelled by a rectangle) are set to 0. And PCA and *eigenfaces* allow, among other things, to restore the degraded area.  


The **objective of this mini-project** is to fill the mask area with a face that is the most similar using PCA and **k nearest neighbours** from the scratch.







## Download the project

You can retrieve this project by running this command on your terminal:
 

     - cd directory_project
     - git clone https://gitlab.com/Sy-Cheikh/covid-mask

## Requirements

 You can run this notebook using Jupyter or Google Collab ....
